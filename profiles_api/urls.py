from django.conf.urls import url
from django.conf.urls import include

from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
# router.register('hello-viewset', views.HelloViewSet, base_name='hello-viewset')
router.register('UserProfile Registration', views.UserProfileViewSet)
router.register('Login', views.LoginViewSet, base_name='login')
router.register('Feed', views.UserProfileFeedViewSet)
router.register('Products',views.ProductViewSet, base_name='product')
router.register('Orders',views.OrdersViewSet,base_name='orders')

urlpatterns = [
    # url(r'^hello-view/', views.HelloApiView.as_view()),
    url(r'', include(router.urls))
]
